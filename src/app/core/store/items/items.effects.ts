import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  addItem, addItemFail,
  addItemSuccess,
  deleteItem,
  deleteItemFail,
  deleteItemSuccess,
  loadItems,
  loadItemsFail,
  loadItemsSuccess, saveItem
} from './items.actions';
import { catchError, concatMap, map, mergeMap, switchMap, withLatestFrom } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Item } from '../../../model/item';
import { of } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../../../app.module';
import { getActiveId } from './items.selectors';

@Injectable()
export class ItemsEffects {

  loadItems$ = createEffect(() => this.actions$.pipe(
    ofType(loadItems),
    switchMap(
      () => this.http.get<Item[]>('https://jsonplaceholder.typicode.com/users')
        .pipe(
          map(items => loadItemsSuccess({ items })),
          catchError(() => of(loadItemsFail()))
        )
    )
  ));

  deleteItem$ = createEffect(() => this.actions$.pipe(
    ofType(deleteItem),
    mergeMap(
      action => this.http.delete('https://jsonplaceholder.typicode.com/users/' + action.id)
        .pipe(
          map(() => deleteItemSuccess({ id: action.id })),
          catchError(() => of(deleteItemFail()))
        )
    )
  ));


  saveItem$ = createEffect(() => this.actions$.pipe(
    ofType(saveItem),
    withLatestFrom(this.store.select(getActiveId)),
    mergeMap(([action, activeId]) => {
      if (activeId) {
        return of(null);
      } else {
        return of(addItem({ item: action.item as any }))
      }
    })
  ));


  addItem$ = createEffect(() => this.actions$.pipe(
    ofType(addItem),
    mergeMap(
      action => this.http.post<Item>('https://jsonplaceholder.typicode.com/users/', action.item)
        .pipe(
          map(item => addItemSuccess({ item })),
          catchError(() => of(addItemFail()))
        )
    )
  ));

  constructor(
    private actions$: Actions,
    private http: HttpClient,
    private store: Store<AppState>
  ) {
  }
}

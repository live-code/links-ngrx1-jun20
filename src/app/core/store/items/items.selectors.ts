import { AppState } from '../../../app.module';
import { createSelector } from '@ngrx/store';
import { Item } from '../../../model/item';

export const getItems = (state: AppState) => state.items.list;
export const getActiveId = (state: AppState) => state.items.active;
export const getItemsError = (state: AppState) => !state.items.isSuccess;

// export const getItemsTotal = (state: AppState) => state.items.list.length;

export const getItemsTotal = createSelector(
  getItems,
  (state: Item[]) => state.length
)

export const getActiveItem = createSelector(
  getItems,
  getActiveId,
  (items: Item[], activeId: number) => items.find(s => s.id === activeId)
)



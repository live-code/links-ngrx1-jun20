import { createReducer, on } from '@ngrx/store';
import { Item } from '../../../model/item';
import * as ItemsActions from './items.actions';

export interface ItemsState {
  list: Item[];
  isSuccess: boolean;
  active: number;
}

const initialState: ItemsState = {
  list: [],
  isSuccess: true,
  active: null
};

export const itemsReducer = createReducer(
  initialState,
  on(ItemsActions.loadItemsSuccess, (state, action) => ({ ...state, list: [...action.items], isSuccess: true })),
  on(ItemsActions.loadItemsFail, (state, action) => ({ ...state, isSuccess: false })),

  on(ItemsActions.addItemSuccess, (state, action) => ({ ...state, list: [...state.list, action.item], isSuccess: true })),
  on(ItemsActions.addItemFail, (state, action) => ({ ...state, isSuccess: false })),

  on(ItemsActions.deleteItemSuccess, (state, action) => ({ ...state, list: state.list.filter(item => item.id !== action.id), isSuccess: true })),
  on(ItemsActions.deleteItemFail, (state, action) => ({ ...state, isSuccess: false })),

  on(ItemsActions.setActive, (state, action) => ({ ...state, active: action.item.id}))
);

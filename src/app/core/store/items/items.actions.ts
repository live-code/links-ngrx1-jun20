import { createAction, props } from '@ngrx/store';
import { Item } from '../../../model/item';


export const loadItems = createAction(
  '[Items] load'
);
export const loadItemsSuccess = createAction(
  '[items] load success',
  props<{ items: Item[]}>()
);
export const loadItemsFail = createAction(
  '[Items] load fail'
);



export const saveItem = createAction(
  '[Items] save',
  props<{ item: Partial<Item>}>()
);

export const addItem = createAction(
  '[Items] add',
  props<{ item: Omit<Item, 'id'>}>()
);
export const addItemSuccess = createAction(
  '[items] add success',
  props<{ item: Item}>()
);
export const addItemFail = createAction(
  '[Items] add fail'
);



export const deleteItem = createAction(
  '[Items] delete',
  props<{ id: number }>()
)
export const deleteItemSuccess = createAction(
  '[items] delete success',
  props<{ id: number }>()
);
export const deleteItemFail = createAction(
  '[Items] delete fail'
);

export const setActive = createAction(
  '[Items] set active',
  props<{ item: Item}>()
);

import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    
    <button routerLink="login">login</button>
    <button routerLink="users">users</button>
    <button routerLink="settings">settings</button>
    <button routerLink="invoices">invoices</button>
    <hr>
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class AppComponent {

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ActionReducerMap, StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { itemsReducer, ItemsState } from './core/store/items/items.reducer';
import { Item } from './model/item';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { EffectsModule } from '@ngrx/effects';
import { ItemsEffects } from './core/store/items/items.effects';

export interface AppState {
  items: ItemsState;
  auth?: { token: string, role: string };
}
export const reducers: ActionReducerMap<AppState> = {
  items: itemsReducer,
  // auth: () => ({ token: null, role: 'Admin'})
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot([ ItemsEffects ]),
    StoreDevtoolsModule.instrument({
      maxAge: 12
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

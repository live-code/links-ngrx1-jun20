export interface Item {
  id: number;
  name: string;
  surname: string;
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InvoicesRoutingModule } from './invoices-routing.module';
import { InvoicesComponent } from './invoices.component';
import { StoreModule } from '@ngrx/store';
import { invoicesFeatureReducers } from './store/reducers';
import { PanelLeftComponent } from './components/panel-left.component';
import { PanelRightComponent } from './components/panel-right.component';


@NgModule({
  declarations: [InvoicesComponent, PanelLeftComponent, PanelRightComponent],
  imports: [
    CommonModule,
    InvoicesRoutingModule,
    StoreModule.forFeature('invoicesPage', invoicesFeatureReducers)
  ]
})
export class InvoicesModule { }

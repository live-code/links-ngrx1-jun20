import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-panel-left',
  template: `
    <div class="side-panel" [ngClass]="{'close': !opened}">
      <h1>{{title}}</h1>
      <button (click)="closed.emit()">close</button>
      
      <ng-content></ng-content>
    </div>
  `,
  styles: [`
    .side-panel {
      position: fixed;
      background-color: #222;
      color: white;
      top: 0;
      bottom: 0;
      left: 0;
      width: 200px;
      overflow: scroll;
      transition: 1s left ease-in-out;
    }
    
    .close {
      left: -200px
    }
    .left {
      
    }
    .right {
      
    }
  `]
})
export class PanelLeftComponent  {
  @Output() closed: EventEmitter<void> = new EventEmitter<void>();
  @Input() title: string;
  @Input() opened = false;
}

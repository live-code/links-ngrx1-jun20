import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-panel-right',
  template: `
    <div class="side-panel" [ngClass]="{'close': !opened}">
      <h1>{{title}}</h1>
      <button (click)="closed.emit()">close</button>
      
      <ng-content></ng-content>
    </div>
  `,
  styles: [`
    .side-panel {
      position: fixed;
      background-color: #222;
      color: white;
      top: 0;
      bottom: 0;
      right: 0;
      width: 200px;
      overflow: scroll;
      transition: 1s right ease-in-out;
    }
    
    .close {
      right: -200px
    }
    
  `]
})
export class PanelRightComponent  {
  @Output() closed: EventEmitter<void> = new EventEmitter<void>();
  @Input() title: string;
  @Input() opened = false;
}

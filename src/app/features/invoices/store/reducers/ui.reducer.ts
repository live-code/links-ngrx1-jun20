import { createReducer, on } from '@ngrx/store';
import { toggleLeftPanel, toggleRightPanel } from '../actions/ui.actions';

export interface UiState {
  panelLeftOpen: boolean;
  panelRightOpen: boolean;
}

const initialState: UiState =  {
  panelLeftOpen: false,
  panelRightOpen: false,
};

export const uiReducer = createReducer(
  initialState,
  on(toggleLeftPanel, ((state, action) => ({ panelRightOpen: false, panelLeftOpen: action.isOpen}))),
  on(toggleRightPanel, ((state, action) => ({ panelLeftOpen: false, panelRightOpen: action.isOpen}))),
);


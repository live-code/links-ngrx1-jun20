import { ActionReducerMap } from '@ngrx/store';
import { invoicesReducer } from './invoices.reducer';
import { clientsReducer } from './clients.reducer';
import { uiReducer, UiState } from './ui.reducer';
import { itemsReducer, ItemsState } from '../../../../core/store/items/items.reducer';

export interface InvoiceFeatureState {
  invoices: any[];
  clients: any[];
  items: ItemsState;
  ui: UiState;
}


export const invoicesFeatureReducers: ActionReducerMap<InvoiceFeatureState> = {
  invoices: invoicesReducer,
  clients: clientsReducer,
  ui: uiReducer,
  items: itemsReducer
}

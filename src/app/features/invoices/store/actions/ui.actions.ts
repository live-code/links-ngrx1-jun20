import { createAction, props } from '@ngrx/store';

export const toggleLeftPanel = createAction(
  '[ui] open left panel',
  props<{ isOpen: boolean }>()
);

export const toggleRightPanel = createAction(
  '[ui] open right panel',
  props<{ isOpen: boolean }>()
);

import { InvoiceFeatureState } from '../reducers';
import { createFeatureSelector, createSelector } from '@ngrx/store';

export const invoiceFeatureSelector = createFeatureSelector('invoicesPage')

export const isOpenLeftPanel = createSelector(
  invoiceFeatureSelector,
  (state: InvoiceFeatureState) => state.ui.panelLeftOpen
);

export const isOpenRightPanel = createSelector(
  invoiceFeatureSelector,
  (state: InvoiceFeatureState) => state.ui.panelRightOpen
);

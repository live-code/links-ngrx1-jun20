import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { toggleLeftPanel, toggleRightPanel } from './store/actions/ui.actions';
import { Observable } from 'rxjs';
import { isOpenLeftPanel, isOpenRightPanel } from './store/selectors/ui.selectors';
import { InvoiceFeatureState } from './store/reducers';

@Component({
  selector: 'app-invoices',
  template: `
    <app-panel-left title="PANEL LEFT" [opened]="isOpenLeftPanel$ | async" (closed)="closeLeft()">
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci assumenda commodi dicta eaque enim hic id laborum maxime, minima molestiae natus, nihil nobis, nostrum nulla perspiciatis qui rerum sequi voluptatum!
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci assumenda commodi dicta eaque enim hic id laborum maxime, minima molestiae natus, nihil nobis, nostrum nulla perspiciatis qui rerum sequi voluptatum!
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci assumenda commodi dicta eaque enim hic id laborum maxime, minima molestiae natus, nihil nobis, nostrum nulla perspiciatis qui rerum sequi voluptatum!
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci assumenda commodi dicta eaque enim hic id laborum maxime, minima molestiae natus, nihil nobis, nostrum nulla perspiciatis qui rerum sequi voluptatum!
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci assumenda commodi dicta eaque enim hic id laborum maxime, minima molestiae natus, nihil nobis, nostrum nulla perspiciatis qui rerum sequi voluptatum!
    </app-panel-left>
    
    <app-panel-right title="right" [opened]="isOpenRightPanel$ | async" (close)="closeRight()">
      bla bla
    </app-panel-right>
    
    <div style="text-align: center">
      <button (click)="openLeft()">APRI LEFT </button>
      <button (click)="openRight()">APRI RIGHT </button>
    </div>
  `,
})
export class InvoicesComponent {
  isOpenLeftPanel$: Observable<boolean> = this.store.pipe(select(isOpenLeftPanel))
  isOpenRightPanel$: Observable<boolean> = this.store.pipe(select(isOpenRightPanel))

  constructor(private store: Store<InvoiceFeatureState>) {
  }

  openLeft(): void {
    this.store.dispatch(toggleLeftPanel({ isOpen: true }))
  }

  openRight(): void {
    this.store.dispatch(toggleRightPanel({ isOpen: true }))
  }

  closeLeft(): void {
    this.store.dispatch(toggleLeftPanel({ isOpen: false }))
  }

  closeRight(): void {
    this.store.dispatch(toggleLeftPanel({ isOpen: false }))
  }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { Item } from '../../model/item';
import { select, Store } from '@ngrx/store';
import { getActiveItem, getItems, getItemsError, getItemsTotal } from '../../core/store/items/items.selectors';
import { NgForm } from '@angular/forms';
import { AppState } from '../../app.module';
import { Actions, ofType } from '@ngrx/effects';
import { addItem, addItemSuccess, deleteItem, loadItems, saveItem, setActive } from '../../core/store/items/items.actions';

@Component({
  selector: 'app-users',
  template: `
    <h1 *ngIf="hasError$ | async">c'è un errore</h1>

    <button (click)="loadItems()">load</button>

    {{getItemsTotal$ | async}} items

    <form #f="ngForm" (submit)="addHandler(f)">
      <input type="text" [ngModel]="(getActiveItem$ | async)?.name" name="name">
      <button>
        {{(getActiveItem$ | async) ? 'EDIT' : 'ADD'}}
      </button>
    </form>

    <li *ngFor="let item of items$ | async" (click)="setActiveHandler(item)">
      {{item.name}} {{item.id}}
      <button (click)="deleteHandler(item.id)">Delete</button>
    </li>

    <pre>{{ getActiveItem$ | async | json }}</pre>

  `,
})
export class UsersComponent {
  items$: Observable<Item[]> = this.store.pipe(select(getItems))
  getItemsTotal$: Observable<number> = this.store.pipe(select(getItemsTotal))
  getActiveItem$: Observable<Item> = this.store.pipe(select(getActiveItem))
  hasError$: Observable<boolean> = this.store.pipe(select(getItemsError))


  @ViewChild('f') form: NgForm;

  constructor(
    private store: Store<AppState>,
    private actions: Actions
  ) {
    actions
      .pipe(
        ofType(addItemSuccess)
      )
      .subscribe(action => {
        this.form.reset();
      });

  }

  loadItems(): void {
    this.store.dispatch(loadItems());
  }

  deleteHandler(id: number): void {
    this.store.dispatch(deleteItem({ id }));
  }

  addHandler(f: NgForm): void {
    this.store.dispatch(saveItem({ item: { name: 'Fabio', surname: 'Biondi'} }));
  }

  setActiveHandler(item: Item): void {
    this.store.dispatch(setActive({ item }));
  }
}
